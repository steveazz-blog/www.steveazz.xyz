# Steve's Blog

<http://www.steve.mt>

## Update Theme

```sh
make update-theme
```

## Local Development

1. Install prerequisites
    - [Nix](https://nixos.org/download/)
    - [devenv](https://devenv.sh/getting-started/)
    - [direnv](https://direnv.net/docs/installation.html#from-system-packages)
1. `direnv` should set up your development environment as soon as you are in the directory
1. Start hugo with:

    ```sh
    devenv up
    ```
